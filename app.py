from flask import Flask, request, make_response, redirect,render_template
import requests
import base64
from ics import Calendar, Event
from urllib.parse import quote_plus
from datetime import datetime, timedelta
import logging

app = Flask(__name__)
app.config.from_object("config.Config")


logging.basicConfig(level=logging.DEBUG)

install_app_url = f"https://zoom.us/oauth/authorize?response_type=code&client_id={app.config.get('CLIENT_ID')}&redirect_uri=https://zm.siriusfrk.ru"

def get_authorization_code():
    return base64.b64encode(
        (app.config.get("CLIENT_ID") + ":" + app.config.get("CLIENT_SECRET")).encode("ascii")).decode("utf8")


def get_token(code):
    access_token_url = f'https://zoom.us/oauth/token?grant_type=authorization_code&code={code}&redirect_uri=https://zm.siriusfrk.ru'
    headers = {
        "Authorization": f"Basic {get_authorization_code()}"
    }
    r = requests.post(access_token_url, headers=headers)
    if r.status_code == 200:
        return r.json()
    else:
        app.logger.info(r.text)
        return None

def update_token(refresh_token):
    access_token_url = f'https://zoom.us/oauth/token?grant_type=refresh_token&refresh_token={refresh_token}'
    headers = {
        "Authorization": f"Basic {get_authorization_code()}"
    }
    r = requests.post(access_token_url, headers=headers)
    if r.status_code == 200:
        return r.json()
    else:
        app.logger.info(r.text)
        return None

def make_ics(zoom_event, description):
    c = Calendar()
    e = Event()
    e.name = zoom_event["topic"]
    e.begin = zoom_event["start_time"]
    e.duration = zoom_event["duration"] / 60 / 24
    e.location = zoom_event["join_url"]
    e.description = description
    c.events.add(e)
    return str(c)

@app.route('/')
def index():
    code = request.args.get('code')
    last_meeting_id = request.cookies.get('last_meeting_id')
    if last_meeting_id is None:
        last_meeting_id = ""
    if code is not None:
        token_resp = get_token(code)
        if token_resp is not None:
            resp = make_response(render_template('index.html', need_install=False, last_meeting_id=last_meeting_id))
            resp.set_cookie("refresh_token", token_resp["refresh_token"])
            return resp
        else:
            return make_response(render_template('index.html', need_install=True, install_app_url=install_app_url, last_meeting_id=last_meeting_id))
    else:
        if request.cookies.get("refresh_token") is None:
            return make_response(render_template('index.html', need_install=True, install_app_url=install_app_url, last_meeting_id=last_meeting_id))
        else:
            return make_response(render_template('index.html', need_install=False))


def get_meeting_info(meetingId):
    meetingId = meetingId.replace(" ", "").replace(".", "").replace("-", "")
    refresh_token = request.cookies.get("refresh_token")

    res_info = None
    res_invitation = None

    if request.cookies.get("refresh_token") is None:
        return redirect('/')
    else:
        r = update_token(refresh_token)
        if r is not None:
            access_token = r["access_token"]
            refresh_token = r["refresh_token"]
            meeting_get_url = f'https://api.zoom.us/v2/meetings/{meetingId}'
            headers = {
                "Authorization": f"Bearer {access_token}"
            }
            r = requests.get(meeting_get_url, headers=headers)
            if r.status_code == 200:
                res_info = r.json()
                meeting_get_invention_url = f'https://api.zoom.us/v2/meetings/{meetingId}/invitation'
                headers = {
                    "Authorization": f"Bearer {access_token}"
                }
                r = requests.get(meeting_get_invention_url, headers=headers)
                if r.status_code == 200:
                    res_invitation = r.json()["invitation"]

    if res_info is None or res_invitation is None:
        return None
    else:
        return {
            "info": res_info,
            "invitation": res_invitation,
            "refresh_token": refresh_token
        }


@app.route('/ics/<meetingId>')
def ics(meetingId):
    r = get_meeting_info(meetingId)
    if r is not None:
        resp = make_response(make_ics(r["info"], r["invitation"]))
        resp.headers['Content-Type'] = 'text/plain;charset=UTF-8'
        resp.headers['Content-Disposition'] = 'attachment;filename=event.ics'
        resp.set_cookie("refresh_token", r["refresh_token"])
        resp.set_cookie("last_meeting_id", meetingId)
        return resp
    else:
        resp = make_response(redirect('/'))
        resp.set_cookie("refresh_token", "", expires=0)
        resp.set_cookie("last_meeting_id", meetingId)
        return resp

def make_gc_url(zoom_event, description):
    # 20140320T221500Z
    name = quote_plus(zoom_event["topic"])
    dt_begin = datetime.strptime(zoom_event["start_time"], "%Y-%m-%dT%H:%M:%SZ")
    dt_end = dt_begin + timedelta(minutes=int(zoom_event["duration"]))
    time_from = dt_begin.strftime("%Y%m%dT%H%M%SZ")
    time_to = dt_end.strftime("%Y%m%dT%H%M%SZ")
    url = zoom_event["join_url"]
    details = quote_plus(description)
    url = f"https://www.google.com/calendar/render?action=TEMPLATE&text={name}&dates={time_from}/{time_to}&details={details}&sprop={url}"
    return redirect(url)

@app.route('/gc/<meetingId>')
def gc(meetingId):
    r = get_meeting_info(meetingId)
    if r is not None:
        resp = make_response(make_gc_url(r["info"], r["invitation"]))
        resp.set_cookie("refresh_token", r["refresh_token"])
        resp.set_cookie("last_meeting_id", meetingId)
        return resp
    else:
        resp = make_response(redirect('/'))
        resp.set_cookie("refresh_token", "", expires=0)
        resp.set_cookie("last_meeting_id", meetingId)
        return resp

@app.route('/policy')
def policy():
    return render_template("policy.html")

@app.route('/terms')
def terms():
    return render_template("terms.html")

@app.route('/support')
def support():
    return render_template("support.html")

@app.route('/zoomverify/verifyzoom.html')
def verifyzoom():
    return render_template("verifyzoom.html")

if __name__ == '__main__':
    app.run()
