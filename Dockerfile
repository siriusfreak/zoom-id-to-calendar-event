FROM ubuntu:latest

ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

RUN apt-get update && apt-get install -y python3-pip python3-dev build-essential libmysqlclient-dev curl iputils-ping git


RUN mkdir /app
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

COPY . /app
WORKDIR /app
ENTRYPOINT flask run --host=0.0.0.0
